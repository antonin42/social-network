# Social Network Project
---

## The project

### Usage

Launch the Back End
```
docker-compose up
```


### Features

- Database
- API
- Mobile Application


#### Database
---
It's a Mongo DB Database, powered by Mongo Atlas.

#### API
---

It's a Python API powered by Flask

#### Mobile App
---

It's a React Native Application

- React Navigation
- React Native UI Kitten
- Axios
- Expo secure store
- React Native Maps
- Expo Location
- React Native QRcode Svg

