// -*-coding:utf-8 -*-

//@File    :   utils.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


export function wait(timeout: number) {
    return new Promise(resolve => setTimeout(resolve, timeout));
}
