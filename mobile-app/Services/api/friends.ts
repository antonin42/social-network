import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import * as SecureStore from "expo-secure-store";

export async function friend_requests_list() {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "get",
    url: "http://172.50.50.20:3000/users/me/friendRequests",
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  if (response.status == 200) {
    return response.data;
  }
  return null;
}

export async function friend_list() {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "get",
    url: "http://172.50.50.20:3000/users/me/friends",
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  if (response.status == 200) {
    return response.data;
  }
  return null;
}

export async function make_friend_request(user_id: string) {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "post",
    url: "http://172.50.50.20:3000/friends/request/" + user_id,
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  return response.status == 200 ? true : false;
}

export async function friend_request_accept(user_id: string) {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "post",
    url: "http://172.50.50.20:3000/friends/accept/" + user_id,
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  return response.status == 200 ? true : false;
}

export async function unfriend(user_id: string) {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "put",
    url: "http://172.50.50.20:3000/friends/unfriend/" + user_id,
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  return response.status == 200 ? true : false;
}
