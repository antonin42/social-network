import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import * as SecureStore from "expo-secure-store";

export async function me_data() {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "get",
    url: "http://172.50.50.20:3000/users/me",
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  if (response.status == 200) {
    return response.data;
  }
  return null;
}

export async function me_posts() {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "get",
    url: "http://172.50.50.20:3000/users/me/posts",
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  if (response.status == 200) {
    return response.data;
  }
  return null;
}

export async function me_likes() {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "get",
    url: "http://172.50.50.20:3000/posts/likes",
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  if (response.status == 200) {
    return response.data;
  }
  return null;
}

export async function me_edit_uri(uri: string) {
  const token = await SecureStore.getItemAsync("secureToken");
  var data = JSON.stringify({
    uri: uri,
  });

  const requestConfig: AxiosRequestConfig = {
    method: "put",
    url: "http://172.50.50.20:3000/users/me",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
    data: data,
  };

  const response: AxiosResponse<Response> = await axios(requestConfig);
  return response.status == 200 ? true : false;
}

export async function me_edit_description(description: string) {
  const token = await SecureStore.getItemAsync("secureToken");
  var data = JSON.stringify({
    description: description,
  });

  const requestConfig: AxiosRequestConfig = {
    method: "put",
    url: "http://172.50.50.20:3000/users/me",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
    data: data,
  };

  const response: AxiosResponse<Response> = await axios(requestConfig);
  return response.status == 200 ? true : false;
}
