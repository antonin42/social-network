// -*-coding:utf-8 -*-

//@File    :   comment.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import * as SecureStore from 'expo-secure-store';

export async function create_comment(post_id: string, comment: string) {
    var token = await SecureStore.getItemAsync('secureToken');

    var data = JSON.stringify({
        "content": comment,
    });

    const requestConfig: AxiosRequestConfig = {
        method: 'post',
        url: 'http://172.50.50.20:3000/posts/' + post_id + '/comment',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        data: data
    };
    const response: AxiosResponse<Response> = await axios(requestConfig);
    return (response.status == 200 ? true : false)
}