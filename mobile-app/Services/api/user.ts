import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import * as SecureStore from "expo-secure-store";

export async function user_data(user_id: string) {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "get",
    url: "http://172.50.50.20:3000/users/" + user_id,
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  if (response.status == 200) {
    return response.data;
  }
  return null;
}
