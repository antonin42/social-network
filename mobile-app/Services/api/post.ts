// -*-coding:utf-8 -*-

//@File    :   post.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import * as SecureStore from 'expo-secure-store';


export async function create_post(title: string | null,
    content: string | null,
    uri: string | null,
    location: string | null,
    latitude: number | null,
    longitude: number | null) {
    const token = await SecureStore.getItemAsync('secureToken');

    var data = JSON.stringify({
        "title": title,
        "content": content,
        "uri": uri,
        "location": location,
        "latitude": latitude,
        "longitude": longitude
    });


    const requestConfig: AxiosRequestConfig = {
        method: 'post',
        url: 'http://172.50.50.20:3000/posts/',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        data: data
    };
    const response: AxiosResponse<Response> = await axios(requestConfig);

    return (response.status == 200 ? true : false)
}

export async function posts_list() {
    const token = await SecureStore.getItemAsync('secureToken');
    const requestConfig: AxiosRequestConfig = {
        method: 'get',
        url: 'http://172.50.50.20:3000/posts/',
        headers: {
            'Authorization': 'Bearer ' + token
        },
    };
    const response: AxiosResponse<Response> = await axios(requestConfig);
    if (response.status == 200) {
        return (response.data);
    }
    return null
}

export async function user_posts_list(user_id: string) {
    const token = await SecureStore.getItemAsync('secureToken');
    const requestConfig: AxiosRequestConfig = {
        method: 'get',
        url: 'http://172.50.50.20:3000/users/' + user_id + '/posts',
        headers: {
            'Authorization': 'Bearer ' + token
        },
    };
    const response: AxiosResponse<Response> = await axios(requestConfig);
    if (response.status == 200) {
        return (response.data);
    }
    return null
}


export async function post_data(post_id: string) {
    const token = await SecureStore.getItemAsync('secureToken');

    const requestConfig: AxiosRequestConfig = {
        method: 'get',
        url: 'http://172.50.50.20:3000/posts/' + post_id,
        headers: {
            'Authorization': 'Bearer ' + token
        },
    };
    const response: AxiosResponse<Response> = await axios(requestConfig);
    if (response.status == 200) {
        return (response.data);
    }
    return null;
}

export async function handle_like(post_id: string, like: boolean) {
    const token = await SecureStore.getItemAsync('secureToken');
    const action = (like ? '/dislike' : '/like')

    const requestConfig: AxiosRequestConfig = {
        method: 'put',
        url: 'http://172.50.50.20:3000/posts/' + post_id + action,
        headers: {
            'Authorization': 'Bearer ' + token
        }
    };
    await axios(requestConfig);
}