import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import * as SecureStore from "expo-secure-store";

export async function messages_list(user_id: string) {
  const token = await SecureStore.getItemAsync("secureToken");
  const requestConfig: AxiosRequestConfig = {
    method: "get",
    url: "http://172.50.50.20:3000/chat/" + user_id,
    headers: {
      Authorization: "Bearer " + token,
    },
  };

  const response: AxiosResponse<Response> = await axios(requestConfig);
  if (response.status == 200) {
    return response.data.messages;
  }
  return null;
}

export async function send_message(user_id: string, content: string) {
  const token = await SecureStore.getItemAsync("secureToken");

  var data = JSON.stringify({
    content: content,
  });

  const requestConfig: AxiosRequestConfig = {
    method: "post",
    url: "http://172.50.50.20:3000/chat/" + user_id,
    headers: {
      Authorization: "Bearer " + token,
      "Content-Type": "application/json",
    },
    data: data,
  };

  const response: AxiosResponse<Response> = await axios(requestConfig);
  return response.status == 200 ? true : false;
}
