import * as SecureStore from "expo-secure-store";

export async function logout(navigation: any) {
  await SecureStore.deleteItemAsync("secureToken");
  navigation.replace("Login");
}
