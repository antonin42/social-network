import * as SecureStore from "expo-secure-store";

export async function verify_token(navigation: any) {
  const token = await SecureStore.getItemAsync("secureToken");
  if (token) navigation.replace("Navbar");
}
