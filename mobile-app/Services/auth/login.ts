import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import * as SecureStore from "expo-secure-store";

export async function login(navigation: any, email: string, password: string) {
  var data = JSON.stringify({
    email: email,
    password: password,
  });

  const requestConfig: AxiosRequestConfig = {
    method: "post",
    url: "http://172.50.50.20:3000/users/login",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  await SecureStore.setItemAsync("secureToken", response.data.access_token);
  navigation.replace("Navbar");
}
