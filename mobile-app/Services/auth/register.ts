import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { login } from "./login";

export async function register(
  navigation: any,
  username: string,
  email: string,
  password: string
) {
  var data = JSON.stringify({
    email: email,
    username: username,
    password: password,
  });

  const requestConfig: AxiosRequestConfig = {
    method: "post",
    url: "http://172.50.50.20:3000/users/register",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };
  const response: AxiosResponse<Response> = await axios(requestConfig);
  if (response.status == 201) {
    login(navigation, email, password);
  }
  return null;
}
