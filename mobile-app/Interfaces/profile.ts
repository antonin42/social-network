export interface IUserProfile {
  username: string;
  email: string;
  uri: string;
  posts: number;
  description: string;
}
