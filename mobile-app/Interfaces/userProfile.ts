// -*-coding:utf-8 -*-

//@File    :   userProfile.ts
//@Author  :   Antonin Alves
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native

export interface IUserProfile {
  username: string;
  uri: string;
  posts: number;
  is_friend: boolean;
  id: string;
}
