
export interface ICreationPostLocation {
    description: string;
}

export interface ICreationPostLocationDetails {
    geometry: {
        location: {
            lat: number,
            lng: number
        }
    }
}

export interface ICreationPostImage {
    uri: string;
}