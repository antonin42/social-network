// -*-coding:utf-8 -*-

//@File    :   router.ts
//@Author  :   Antonin Alves
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native

export type RootStackParamsList = {
  Home: undefined;
  Post: undefined;
  Profile: undefined;
  Scan: undefined;
  Navbar: undefined;
  Register: undefined;
  PostPage: undefined;
  Map: undefined;
  Login: undefined;
  UserProfile: undefined;
  Chat: undefined;
  ChatRoom: undefined;
};
