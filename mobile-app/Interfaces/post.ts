// -*-coding:utf-8 -*-

//@File    :   post.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


export interface IPost {
    id: string;
    title: string;
    author: string;
    author_username: string;
    uri: string;
    content: string;
    created: {
        $date: string;
    }
    like: number;
    liked: boolean;
    comments: [{
        author: string;
        author_username: string;
        content: string;
    }]
    location: {
        description: string;
        latitude: number;
        longitude: number;
    }
}
