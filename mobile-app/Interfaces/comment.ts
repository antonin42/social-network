

export interface IComment {
    author: string;
    author_username: string;
    uri: string;
    content: string;
    created: {
        $date: string;
    }
}
