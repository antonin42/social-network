// Import Libs
import React, { useEffect } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  RefreshControl,
  Modal,
} from "react-native";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import SegmentedControlTab from "react-native-segmented-control-tab";
import QRCode from "react-native-qrcode-svg";
import * as ImagePicker from "expo-image-picker";
import {
  Layout,
  Text,
  Button,
  Icon,
  Divider,
  Input,
} from "@ui-kitten/components";

// Import Services
import { me_data, me_posts, me_likes } from "../Services/api/me";
import { wait } from "../Services/utils";
import { logout } from "../Services/auth/logout";
import { me_edit_uri, me_edit_description } from "../Services/api/me";
// Import Interfaces
import { IUserProfile } from "../Interfaces/profile";
import { RootStackParamsList } from "../Interfaces/router";

// Import Components
import { Post } from "../Components/Post";

// Import StyleSheets
import styles from "../StyleSheets/ProfileScreen";

type ProfileScreenProps = NativeStackScreenProps<RootStackParamsList, "Home">;

export const ProfileScreen: React.FC<ProfileScreenProps> = (props) => {
  const [image, setImage] = React.useState(null);
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const [posts, setPosts] = React.useState([]);
  const [likes, setLikes] = React.useState([]);
  const [refreshing, setRefreshing] = React.useState(false);
  const [modalVisible, setModalVisible] = React.useState(false);
  const [profileData, setProfilData] = React.useState<IUserProfile | null>(
    null
  );
  const [editDescription, setEditDescription] = React.useState<boolean>(false);
  const [description, setDescription] = React.useState();
  useEffect(() => {
    const setup = async () => {
      setProfilData(await me_data());
      setPosts(await me_posts());
      setLikes(await me_likes());
    };
    setup();
  }, []);

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    setPosts(await me_posts());
    setLikes(await me_likes());
    // setDescription(profileData.description);
    wait(1000).then(() => setRefreshing(false));
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
      await me_edit_uri(result.uri);
      setProfilData(await me_data());
    }
  };

  return (
    <Layout style={styles.container}>
      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            {profileData && <QRCode size={250} value={profileData.id} />}
            <Button
              style={styles.modalCloseBtn}
              size="small"
              onPress={() => setModalVisible(!modalVisible)}
            >
              Close
            </Button>
          </View>
        </View>
      </Modal>
      <View style={styles.title}>
        <Text category="h2" style={styles.title_text}>
          Profile
        </Text>
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity onPress={() => setModalVisible(true)}>
            <Icon style={styles.icon} fill="#FFF" name="grid-outline" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => logout(props.navigation)}>
            <Icon style={styles.icon} fill="#FFF" name="log-out" />
          </TouchableOpacity>
        </View>
      </View>
      <Layout>
        <View style={styles.header_profile}>
          <TouchableOpacity onPress={pickImage}>
            <Image
              source={{
                uri:
                  profileData && profileData.uri
                    ? profileData.uri
                    : "https://wallpapercave.com/wp/wp4041617.jpg",
              }}
              style={styles.profile_pic}
            />
          </TouchableOpacity>
          <View>
            <Text style={styles.profile_username}>
              {profileData ? profileData.username : "someone"}
            </Text>
            <Text style={styles.profile_post_number}>
              {profileData && profileData.posts ? profileData.posts : "0"} posts
            </Text>
            <Text style={styles.profile_email}>
              {profileData ? profileData.email : "someone@somewhat.com"}
            </Text>
          </View>
        </View>
        <View>
          {!editDescription && (
            <View style={styles.description_container}>
              <Text style={styles.description_text}>
                {profileData && profileData.description}
              </Text>
              <TouchableOpacity onPress={() => setEditDescription(true)}>
                <Icon style={styles.icon} fill="#FFF" name="edit-outline" />
              </TouchableOpacity>
            </View>
          )}
          {editDescription && (
            <View style={styles.description_container}>
              <Input
                multiline={true}
                value={description}
                onChangeText={(nextValue) => setDescription(nextValue)}
              />
              <TouchableOpacity
                onPress={async () => {
                  await me_edit_description(description);
                  setProfilData(await me_data());
                  setEditDescription(false);
                }}
              >
                <Icon
                  style={styles.icon}
                  fill="#FFF"
                  name="checkmark-outline"
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
        <Divider
          style={{
            backgroundColor: "#AAA",
            marginLeft: 30,
            marginRight: 30,
            marginTop: 10,
          }}
        />
        <SegmentedControlTab
          tabStyle={styles.tabStyle}
          tabTextStyle={styles.tabTextStyle}
          activeTabStyle={styles.activeTabStyle}
          activeTabTextStyle={styles.activeTabTextStyle}
          values={["POSTS", "LIKES"]}
          selectedIndex={selectedIndex}
          onTabPress={(index) => setSelectedIndex(index)}
        />
      </Layout>
      <Layout style={styles.layout}>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          {selectedIndex == 0
            ? posts.map((data) => (
                <Post
                  navigation={props.navigation}
                  data={data}
                  key={data._id.$oid}
                />
              ))
            : likes.map((data) => (
                <Post
                  navigation={props.navigation}
                  data={data}
                  key={data._id.$oid}
                />
              ))}
        </ScrollView>
      </Layout>
    </Layout>
  );
};
