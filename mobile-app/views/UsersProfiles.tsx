// -*-coding:utf-8 -*-

//@File    :   UsersProfiles.tsx
//@Author  :   Antonin Alves
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native

import React, { useEffect } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  RefreshControl,
} from "react-native";
import { Layout, Text, Icon } from "@ui-kitten/components";
import { Divider } from "react-native-paper";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import Services
import { user_data } from "../Services/api/user";
import { user_posts_list } from "../Services/api/post";
import { make_friend_request, unfriend } from "../Services/api/friends";

//Import Interfaces
import { IUserProfile } from "../Interfaces/userProfile";
import { RootStackParamsList } from "../Interfaces/router";

// Import StyleSheets
import styles from "../StyleSheets/UserProfile";

// Import Components
import { Post } from "../Components/Post";

type UserProfileScreenProps = NativeStackScreenProps<
  RootStackParamsList,
  "UserProfile"
>;

export const UserProfileScreen: React.FC<UserProfileScreenProps> = (props) => {
  const [posts, setPosts] = React.useState([]);
  const [refreshing, setRefreshing] = React.useState(false);
  const [profileData, setProfilData] = React.useState<IUserProfile | null>(
    null
  );
  const { goBack } = props.navigation;

  useEffect(() => {
    const setup = async () => {
      setProfilData(await user_data(props.route.params.data));
      setPosts(await user_posts_list(props.route.params.data));
    };
    setup();
  }, []);

  const wait = (timeout) => {
    return new Promise((resolve) => setTimeout(resolve, timeout));
  };

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    setPosts(await user_posts_list(props.route.params.data));
    wait(1000).then(() => setRefreshing(false));
  }, []);

  return (
    <Layout style={styles.container}>
      <View>
        <View style={styles.header_page}>
          <TouchableOpacity onPress={goBack}>
            <Icon style={styles.icon} fill="#FFF" name="arrow-back" />
          </TouchableOpacity>
        </View>
        <View style={styles.header_profile}>
          <Image
            source={{
              uri:
                profileData && profileData.uri
                  ? profileData.uri
                  : "https://wallpapercave.com/wp/wp4041617.jpg",
            }}
            style={styles.profile_pic}
          />
          <View>
            <Text style={styles.profile_username}>
              {profileData ? profileData.username : "someone"}
            </Text>
            <Text style={styles.profile_post_number}>
              {profileData && profileData.posts ? profileData.posts : "0"} posts
            </Text>
          </View>
        </View>
        <View style={styles.friend_request}>
          {profileData && profileData.is_friend ? (
            <TouchableOpacity
              style={styles.friend_request_btn}
              onPress={async () => {
                await unfriend(profileData.id);
              }}
            >
              <Text style={styles.friend_request_btn_text}>Unfriend</Text>
            </TouchableOpacity>
          ) : (
            profileData && (
              <TouchableOpacity
                style={styles.friend_request_btn}
                onPress={() => {
                  make_friend_request(profileData.id);
                }}
              >
                <Text style={styles.friend_request_btn_text}>
                  friend request
                </Text>
              </TouchableOpacity>
            )
          )}
        </View>
      </View>

      <Layout>
        <Divider
          style={{
            backgroundColor: "#AAA",
            marginLeft: 30,
            marginRight: 30,
            marginTop: 10,
          }}
        />
      </Layout>
      <Layout style={styles.layout}>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          {posts.map((data) => (
            <Post
              navigation={props.navigation}
              data={data}
              key={data._id.$oid}
            />
          ))}
        </ScrollView>
      </Layout>
    </Layout>
  );
};
