// Import Libs
import React, { useState, useEffect } from "react";
import { View, StyleSheet } from "react-native";
import { Text, Button } from "@ui-kitten/components";
import { BarCodeScanner } from "expo-barcode-scanner";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import Interfaces
import { RootStackParamsList } from "../Interfaces/router";

// Import StyleSheets
import styles from "../StyleSheets/ScanScreen";

type ScanScreenProps = NativeStackScreenProps<RootStackParamsList, "Home">;

export const ScanScreen: React.FC<ScanScreenProps> = (props) => {
  const [hasPermission, setHasPermission] = useState<boolean | null>(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    props.navigation.push("UserProfile", {
      data: data,
    });
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      {scanned && (
        <Button style={styles.button} onPress={() => setScanned(false)}>
          SCAN
        </Button>
      )}
    </View>
  );
};
