// Import Libs
import React from "react";
import { TouchableOpacity } from "react-native";
import { Layout, Text, Input } from "@ui-kitten/components";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import Services
import { register } from "../Services/auth/register";

// Import Interfaces
import { RootStackParamsList } from "../Interfaces/router";

// Import StyleSheets
import styles from "../StyleSheets/LoginScreen";

type registerScreenProps = NativeStackScreenProps<
  RootStackParamsList,
  "Register"
>;

export const RegisterScreen: React.FC<registerScreenProps> = (props) => {
  const [username, setUsername] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  return (
    <Layout style={styles.container}>
      <Layout style={styles.layout}>
        <Layout style={styles.header}>
          <Text style={styles.headerText}>Social Network</Text>
        </Layout>
        <Layout style={styles.form}>
          <Layout>
            <Input
              placeholder="Username"
              style={styles.inputs}
              value={username}
              onChangeText={(nextValue) => setUsername(nextValue)}
            />
          </Layout>
          <Layout>
            <Input
              placeholder="Email"
              style={styles.inputs}
              value={email}
              onChangeText={(nextValue) => setEmail(nextValue)}
            />
          </Layout>
          <Layout>
            <Input
              placeholder="Password"
              style={styles.inputs}
              value={password}
              onChangeText={(nextValue) => setPassword(nextValue)}
            />
          </Layout>
          <TouchableOpacity
            style={styles.formBtn}
            onPress={() =>
              register(props.navigation, username, email, password)
            }
          >
            <Text style={styles.formBtnText}>Register</Text>
          </TouchableOpacity>
        </Layout>

        <Layout style={styles.bottomBtnContainer}>
          <TouchableOpacity
            style={styles.bottomBtn}
            onPress={() => props.navigation.navigate("Login")}
          >
            <Text style={styles.bottomBtnText}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.bottomBtn}
            onPress={() => props.navigation.navigate("Register")}
          >
            <Text style={styles.bottomBtnText}>Register</Text>
          </TouchableOpacity>
        </Layout>
      </Layout>
    </Layout>
  );
};
