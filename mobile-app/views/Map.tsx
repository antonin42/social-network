
//Import Libs
import React, { useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Text, Button, Icon } from '@ui-kitten/components';
import MapView, { Marker } from 'react-native-maps';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

//Import Services
import { post_data } from '../Services/api/post';

//Import Interfaces
import { IPost } from '../Interfaces/post';
import { RootStackParamsList } from '../Interfaces/router';

//Import StyleSheets
import styles from '../StyleSheets/MapScreen'

type MapScreenProps = NativeStackScreenProps<RootStackParamsList, 'Map'>;

export const MapScreen: React.FC<MapScreenProps> = (props) => {
    const { goBack } = props.navigation;
    const [post, setPost] = React.useState<IPost | null>(null)

    useEffect(() => {
        const setup = async () => {
            setPost(await post_data(props.route.params.data))
        }
        console.log(props);

        setup()
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.post_title}>{post ? post.title : 'title'}</Text>
                <Text style={styles.post_author}>by {post ? post.author_username : 'someone'}</Text>
                <View style={styles.post_location_icon}>
                    <Icon
                        style={styles.post_icon}
                        fill='#FFF'
                        name='compass-outline'
                    />
                    <Text style={styles.post_location}>from {post ? post.location.description : 'somewhere'}</Text>
                </View>
            </View>
            {post &&
                <MapView
                    style={styles.map}
                    initialRegion={{
                        latitude: post.location.latitude,
                        longitude: post.location.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                >
                    <Marker
                        coordinate={{ latitude: post.location.latitude, longitude: post.location.longitude }}
                    />
                </MapView>}
            <TouchableOpacity style={styles.map_btn} onPress={goBack}>
                <Icon
                    style={styles.map_btn_icon}
                    fill='#FFF'
                    name='arrow-back'
                />
                <Text>EXIT MAP</Text>
            </TouchableOpacity>
        </View>

    )
}
