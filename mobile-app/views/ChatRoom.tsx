// -*-coding:utf-8 -*-

//@File    :   ChatRoom.tsx
//@Author  :   Antonin Alves
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native

//Import Libs
import React, { useEffect } from "react";
import {
  ScrollView,
  RefreshControl,
  View,
  TouchableOpacity,
  Keyboard,
} from "react-native";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { Layout, Text, Icon, Input } from "@ui-kitten/components";
import { GiftedChat } from "react-native-gifted-chat";
import io from "react-native-socket.io-client";
// Import Services
import { wait } from "../Services/utils";

// Import Interfaces
import { RootStackParamsList } from "../Interfaces/router";

// Import Components

// Import StyleSheets
import styles from "../StyleSheets/ChatRoomScreen";
import { messages_list, send_message } from "../Services/chat/chat";

type ChatRoomScreenProps = NativeStackScreenProps<RootStackParamsList, "Home">;

export const ChatRoomScreen: React.FC<ChatRoomScreenProps> = (props) => {
  // Initiation

  const [refreshing, setRefreshing] = React.useState(false);
  const [messages, setMessages] = React.useState([]);
  const [new_message, setNewMessage] = React.useState("");

  const parse_messages = async () => {
    // setMessages([]);
    // const messages_ = await messages_list(props.route.params.data);
    // console.log(messages_);

    setMessages(await messages_list(props.route.params.data));

    // messages_.messages.reverse().map((data) =>
    //   setMessages((messages) => [
    //     ...messages,
    //     {
    //       _id: data.user_id == props.route.params.data ? 1 : 2,
    //       text: data.content,
    //       createdAt: data.created.$date,
    //       user: {
    //         _id: data.user_id == props.route.params.data ? 2 : 1,
    //         name: data.username,
    //         avatar: data.user_uri,
    //       },
    //     },
    //   ])
    // );
  };

  useEffect(() => {
    const setup = async () => {
      parse_messages();
      console.log(messages);
    };
    setup();
  }, []);

  // Refresh Function

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    wait(1000).then(() => setRefreshing(false));
  }, []);

  const onSend = async () => {
    Keyboard.dismiss();
    await send_message(props.route.params.data, new_message);
    setMessages(await messages_list(props.route.params.data));
    setNewMessage("");
  };

  // View Body

  return (
    <Layout style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.title_text} category="h2">
          Social
        </Text>
        <TouchableOpacity onPress={() => props.navigation.push("Chat")}>
          <Icon style={styles.icon} fill="#FFF" name="arrow-back" />
        </TouchableOpacity>
      </View>
      <Layout style={styles.layout}>
        <Layout style={{ minHeight: 100, marginTop: 0 }}>
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          >
            {messages.map((data) =>
              data.user_id == props.route.params.data ? (
                <View style={styles.left_message}>
                  <View style={styles.left_message_buble}>
                    <Text style={styles.left_message_text}>{data.content}</Text>
                  </View>
                </View>
              ) : (
                <View style={styles.right_message}>
                  <View style={styles.right_message_buble}>
                    <Text style={styles.right_message_text}>
                      {data.content}
                    </Text>
                  </View>
                </View>
              )
            )}
            <View style={{ height: 100 }} />
          </ScrollView>
          <View style={styles.new_message}>
            <Input
              placeholder="Comment this post"
              multiline={true}
              style={styles.new_message_input}
              value={new_message}
              onChangeText={(nextValue) => setNewMessage(nextValue)}
            />
            <TouchableOpacity onPress={onSend}>
              <Icon
                style={styles.new_message_icon}
                fill="#FFF"
                name={"message-square"}
              />
            </TouchableOpacity>
          </View>
        </Layout>
      </Layout>
    </Layout>
  );
};
