// Import Libs
import React, { useEffect } from "react";
import { TouchableOpacity } from "react-native";
import { Layout, Text, Input } from "@ui-kitten/components";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import Services
import { login } from "../Services/auth/login";
import { verify_token } from "../Services/auth/token";

// Import Interfaces
import { RootStackParamsList } from "../Interfaces/router";

// Import StyleSheets
import styles from "../StyleSheets/LoginScreen";

type LoginScreenProps = NativeStackScreenProps<RootStackParamsList, "Login">;

export const LoginScreen: React.FC<LoginScreenProps> = (props) => {
  const [email, setEmail] = React.useState("antonin@lors-la.center");
  const [password, setPassword] = React.useState("Password1!");

  useEffect(() => {
    verify_token(props.navigation);
  }, []);

  return (
    <Layout style={styles.container}>
      <Layout style={styles.layout}>
        <Layout style={styles.header}>
          <Text style={styles.headerText}>Social Network</Text>
        </Layout>
        <Layout style={styles.form}>
          <Layout>
            <Input
              placeholder="email"
              style={styles.inputs}
              value={email}
              onChangeText={(nextValue) => setEmail(nextValue)}
            />
          </Layout>
          <Layout>
            <Input
              placeholder="password"
              style={styles.inputs}
              value={password}
              onChangeText={(nextValue) => setPassword(nextValue)}
            />
          </Layout>
          <TouchableOpacity
            style={styles.formBtn}
            onPress={() => login(props.navigation, email, password)}
          >
            <Text style={styles.formBtnText}>Login</Text>
          </TouchableOpacity>
        </Layout>

        <Layout style={styles.bottomBtnContainer}>
          <TouchableOpacity
            style={styles.bottomBtn}
            onPress={() => props.navigation.navigate("Login")}
          >
            <Text style={styles.bottomBtnText}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.bottomBtn}
            onPress={() => props.navigation.navigate("Register")}
          >
            <Text style={styles.bottomBtnText}>Register</Text>
          </TouchableOpacity>
        </Layout>
      </Layout>
    </Layout>
  );
};
