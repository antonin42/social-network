// -*-coding:utf-8 -*-

//@File    :   Home.tsx
//@Author  :   Antonin Alves
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native

//Import Libs
import React, { useEffect } from "react";
import {
  ScrollView,
  RefreshControl,
  View,
  TouchableOpacity,
} from "react-native";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { Layout, Text, Icon } from "@ui-kitten/components";

// Import Services
import { posts_list } from "../Services/api/post";
import { wait } from "../Services/utils";

// Import Interfaces
import { RootStackParamsList } from "../Interfaces/router";

// Import Components
import { Post } from "../Components/Post";

// Import StyleSheets
import styles from "../StyleSheets/HomeScreen";

type HomeScreenProps = NativeStackScreenProps<RootStackParamsList, "Home">;

export const HomeScreen: React.FC<HomeScreenProps> = (props) => {
  // Initiation

  const [posts, setPosts] = React.useState([]);
  const [refreshing, setRefreshing] = React.useState(false);

  useEffect(() => {
    const setup = async () => {
      setPosts(await posts_list());
    };
    setup();
  }, []);

  // Refresh Function

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    setPosts(await posts_list());
    wait(1000).then(() => setRefreshing(false));
  }, []);

  // View Body

  return (
    <Layout style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.title_text} category="h2">
          Social
        </Text>
        <TouchableOpacity onPress={() => props.navigation.push("Chat")}>
          <Icon style={styles.icon} fill="#FFF" name="paper-plane-outline" />
        </TouchableOpacity>
      </View>
      <Layout style={styles.layout}>
        <Layout style={{ minHeight: 100, marginTop: 0 }}>
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          >
            {posts.reverse().map((data) => (
              <Post
                navigation={props.navigation}
                data={data}
                key={data._id.$oid}
              />
            ))}
          </ScrollView>
        </Layout>
      </Layout>
    </Layout>
  );
};
