import React, { useEffect } from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { Layout, Text, Input, Button, Icon } from "@ui-kitten/components";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as SecureStore from "expo-secure-store";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import SegmentedControlTab from "react-native-segmented-control-tab";

// Import Services
import {
  friend_requests_list,
  friend_list,
  friend_request_accept,
  unfriend,
} from "../Services/api/friends";
// Import Interfaces
import { RootStackParamsList } from "../Interfaces/router";

// Import StyleSheets
import styles from "../StyleSheets/ChatScreen";
import { Divider } from "react-native-paper";

type chatScreenProps = NativeStackScreenProps<RootStackParamsList, "Chat">;

export const ChatScreen: React.FC<chatScreenProps> = (props) => {
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const [friendRequests, setFriendRequests] = React.useState([]);
  const [friends, setFriends] = React.useState([]);

  const { goBack } = props.navigation;

  useEffect(() => {
    const setup = async () => {
      setFriendRequests(await friend_requests_list());
      setFriends(await friend_list());
    };
    setup();
  }, []);

  return (
    <Layout style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.title_text} category="h2">
          Chat
        </Text>
        <TouchableOpacity
          onPress={() => {
            props.navigation.push("Navbar");
          }}
        >
          <Icon style={styles.icon} fill="#FFF" name="arrow-back" />
        </TouchableOpacity>
      </View>
      <View>
        <SegmentedControlTab
          tabStyle={styles.tabStyle}
          tabTextStyle={styles.tabTextStyle}
          activeTabStyle={styles.activeTabStyle}
          activeTabTextStyle={styles.activeTabTextStyle}
          values={["CHATS", "FRIENDS"]}
          selectedIndex={selectedIndex}
          onTabPress={(index) => setSelectedIndex(index)}
        />
      </View>
      {selectedIndex == 0 ? (
        <View style={styles.friends_container}>
          <Text style={styles.friends_title}>Start Chatting</Text>
          {friends.length > 0 ? (
            friends.map((data) => (
              <View style={styles.profile_container}>
                <View style={styles.profile_header}>
                  <Image
                    source={{
                      uri: props.uri
                        ? props.uri
                        : "https://wallpapercave.com/wp/wp4041617.jpg",
                    }}
                    style={styles.profile_pic}
                  />

                  <Text style={styles.profile_username}>{data.username}</Text>
                </View>
                <TouchableOpacity
                  onPress={async () => {
                    props.navigation.navigate("ChatRoom", {
                      data: data.user_id,
                    });
                  }}
                >
                  <Icon
                    style={styles.icon}
                    fill="#FFF"
                    name="message-square-outline"
                  />
                </TouchableOpacity>
              </View>
            ))
          ) : (
            <Text>You don't have friend to Chat with :(</Text>
          )}
        </View>
      ) : (
        <View>
          <View style={styles.friend_request_container}>
            <Text style={styles.friend_request_title}>Friend Requests</Text>
            {friendRequests.length > 0 ? (
              friendRequests.map((data) => (
                <View style={styles.profile_container}>
                  <View style={styles.profile_header}>
                    <Image
                      source={{
                        uri: props.uri
                          ? props.uri
                          : "https://wallpapercave.com/wp/wp4041617.jpg",
                      }}
                      style={styles.profile_pic}
                    />

                    <Text style={styles.profile_username}>{data.username}</Text>
                  </View>
                  <TouchableOpacity
                    onPress={async () => {
                      await friend_request_accept(data.user_id);
                      setFriends(await friend_list());
                      setFriendRequests(await friend_requests_list());
                    }}
                  >
                    <Icon
                      style={styles.icon}
                      fill="#FFF"
                      name="plus-square-outline"
                    />
                  </TouchableOpacity>
                </View>
              ))
            ) : (
              <Text>You don't have firend requests</Text>
            )}
          </View>
          <View style={styles.friends_container}>
            <Text style={styles.friends_title}>Friends</Text>
            {friends.length > 0 ? (
              friends.map((data) => (
                <View style={styles.profile_container}>
                  <View style={styles.profile_header}>
                    <Image
                      source={{
                        uri: props.uri
                          ? props.uri
                          : "https://wallpapercave.com/wp/wp4041617.jpg",
                      }}
                      style={styles.profile_pic}
                    />

                    <Text style={styles.profile_username}>{data.username}</Text>
                  </View>
                  <TouchableOpacity
                    onPress={async () => {
                      await unfriend(data.user_id);
                      setFriends(await friend_list());
                      setFriendRequests(await friend_requests_list());
                    }}
                  >
                    <Icon
                      style={styles.icon}
                      fill="#FFF"
                      name="trash-2-outline"
                    />
                  </TouchableOpacity>
                </View>
              ))
            ) : (
              <Text>You don't have friend :(</Text>
            )}
          </View>
        </View>
      )}
    </Layout>
  );
};
