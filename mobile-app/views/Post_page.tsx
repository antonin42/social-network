// Import Libs
import React, { useEffect } from "react";
import {
  Image,
  ScrollView,
  RefreshControl,
  View,
  TouchableOpacity,
  Keyboard,
} from "react-native";
import { Layout, Text, Input, Divider, Icon } from "@ui-kitten/components";

// Import Services
import { post_data } from "../Services/api/post";
import { create_comment } from "../Services/api/comment";
import { wait } from "../Services/utils";

// Import Interfaces
import { IPost } from "../Interfaces/post";

// Import Components
import { CommentComponent } from "../Components/Comment";

// Import StyleSheets
import styles from "../StyleSheets/PostPage";

export const Post_page = (props: any) => {
  const [refreshing, setRefreshing] = React.useState(false);
  const [comment, setComment] = React.useState("");
  const [post, setPost] = React.useState<IPost[]>([]);
  const { goBack } = props.navigation;

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    setPost(await post_data(props.route.params.data.id));
    wait(1000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    const setup = async () => {
      setPost(await post_data(props.route.params.data.id));
    };
    setup();
  }, []);

  return (
    <Layout style={styles.container}>
      <View style={styles.title}>
        <Text category="h2" style={styles.title_text}>
          Comment
        </Text>
        <TouchableOpacity onPress={goBack}>
          <Icon style={styles.icon} fill="#FFF" name="arrow-back" />
        </TouchableOpacity>
      </View>

      <Layout>
        <View style={styles.profile_header}>
          <View style={styles.profile_pic_username}>
            <Image
              source={{
                uri: props.uri
                  ? props.uri
                  : "https://wallpapercave.com/wp/wp4041617.jpg",
              }}
              style={styles.profile_pic}
            />

            <Text style={styles.profile_username}>
              {" "}
              {props.route.params.data.author_username}{" "}
            </Text>
          </View>
          <Text style={styles.post_title}>
            {" "}
            {props.route.params.data.title}{" "}
          </Text>
          <Text style={styles.post_content}>
            {" "}
            {props.route.params.data.content}{" "}
          </Text>
          <Divider style={{ backgroundColor: "#222", margin: 15 }} />
        </View>
        <Layout>
          <View style={styles.new_comment}>
            <Input
              placeholder="Comment this post"
              multiline={true}
              style={styles.new_comment_input}
              value={comment}
              onChangeText={(nextValue) => setComment(nextValue)}
            />
            <TouchableOpacity
              onPress={() => {
                Keyboard.dismiss();
                setComment("");
                create_comment(props.route.params.data.id, comment);
              }}
            >
              <Icon
                style={styles.new_comment_icon}
                fill="#FFF"
                name={"message-square"}
              />
            </TouchableOpacity>
          </View>
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          >
            {post.comments &&
              post.comments
                .reverse()
                .map((data) => (
                  <CommentComponent
                    content={data.content}
                    author_username={data.author_username}
                  />
                ))}
          </ScrollView>
        </Layout>
      </Layout>
    </Layout>
  );
};
