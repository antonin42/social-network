// Import Libs
import React, { useEffect } from "react";
import { Keyboard, Image, View, TouchableOpacity } from "react-native";
import { Layout, Text, Input, Icon, Card, Modal } from "@ui-kitten/components";
import * as ImagePicker from "expo-image-picker";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import Services
import { create_post } from "../Services/api/post";

//Import Interfaces
import {
  ICreationPostLocation,
  ICreationPostLocationDetails,
  ICreationPostImage,
} from "../Interfaces/createPost";
import { RootStackParamsList } from "../Interfaces/router";

// Import StyleSheets
import styles from "../StyleSheets/CreatePostScreen";

type CreatePostScreenProps = NativeStackScreenProps<
  RootStackParamsList,
  "Post"
>;

export const PostScreen: React.FC<CreatePostScreenProps> = () => {
  const [title, setTitle] = React.useState<string | null>(null);
  const [content, setContent] = React.useState<string | null>(null);
  const [image, setImage] = React.useState(null);
  const [visible, setVisible] = React.useState<boolean>(false);

  const [location, setLocation] = React.useState<ICreationPostLocation | null>(
    null
  );
  const [locationDetail, setLocationDetail] =
    React.useState<ICreationPostLocationDetails | null>(null);

  useEffect(() => {}, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  return (
    <Layout style={styles.container}>
      <Modal
        visible={visible}
        backdropStyle={styles.backdrop}
        onBackdropPress={() => setVisible(false)}
      >
        <Card disabled={true}>
          <View style={styles.map}>
            <GooglePlacesAutocomplete
              placeholder="Search destination"
              minLength={2}
              fetchDetails={true}
              onPress={(data, details = null) => {
                setLocation(data);
                setLocationDetail(details);
                setVisible(false);
                console.log(data, details);
              }}
              query={{
                key: "AIzaSyABzvk2IM6CSzoUQV56LvoJIKEnRKAusf0",
                language: "en",
              }}
            />

            <TouchableOpacity
              onPress={() => setVisible(false)}
              style={styles.map_btn}
            >
              <Text style={styles.map_btn_text}>Quit</Text>
            </TouchableOpacity>
          </View>
        </Card>
      </Modal>
      <View style={styles.title}>
        <Text category="h2" style={styles.title_text}>
          Make a post
        </Text>
      </View>
      <Layout style={styles.form}>
        <Layout>
          <Input
            placeholder="Add a title"
            textStyle={{ color: "#FFF" }}
            placeholderTextColor="#EEE"
            style={styles.inputs}
            value={title}
            onChangeText={(nextValue) => setTitle(nextValue)}
          />
        </Layout>
        <Layout>
          <Input
            placeholder="Add a description"
            textStyle={{ color: "#FFF" }}
            placeholderTextColor="#EEE"
            style={styles.inputs}
            value={content}
            onChangeText={(nextValue) => setContent(nextValue)}
          />
        </Layout>
        <Layout>
          <TouchableOpacity onPress={pickImage} style={styles.pick_image}>
            {image ? (
              <Image source={{ uri: image }} style={styles.image} />
            ) : (
              <Icon style={styles.icon_image} fill="#FFF" name="plus" />
            )}
          </TouchableOpacity>
        </Layout>
        <Layout style={{ flexDirection: "row" }}>
          {location ? (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Text style={{ color: "#fff", marginLeft: 20 }}>
                {location.description}
              </Text>
              <TouchableOpacity onPress={() => setLocation(null)}>
                <Icon style={styles.icon} fill="#FFF" name="close-outline" />
              </TouchableOpacity>
            </View>
          ) : (
            <TouchableOpacity
              style={styles.btn}
              onPress={() => setVisible(true)}
            >
              <Text style={styles.btn_text}>Add Location</Text>
            </TouchableOpacity>
          )}
        </Layout>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => {
            Keyboard.dismiss();
            setTitle(null);
            setContent(null);
            setImage(null);
            create_post(
              title,
              content,
              image,
              location ? location.description : null,
              locationDetail ? locationDetail.geometry.location.lat : null,
              locationDetail ? locationDetail.geometry.location.lng : null
            );
          }}
        >
          <Text style={styles.btn_text}>Post</Text>
        </TouchableOpacity>
      </Layout>
    </Layout>
  );
};
