import React, { useEffect } from "react";
import { StyleSheet } from "react-native";

import { StatusBar } from "expo-status-bar";
import * as eva from "@eva-design/eva";
import { ApplicationProvider, IconRegistry, Icon } from "@ui-kitten/components";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import * as SecureStore from "expo-secure-store";
import { default as theme } from "./theme.json"; // <-- Import app theme
import axios from "axios";
import { EvaIconsPack } from "@ui-kitten/eva-icons";

import { LoginScreen } from "./views/Login";
import { RegisterScreen } from "./views/Register";
import { HomeScreen } from "./views/Home";
import { BottomBar } from "./Components/bottomBar";
import { ChatScreen } from "./views/Chat";
import { ProfileScreen } from "./views/Profile";
import { PostScreen } from "./views/Post";
import { Post_page } from "./views/Post_page";
import { MapScreen } from "./views/Map";
import { UserProfileScreen } from "./views/UsersProfiles";
import { ScanScreen } from "./views/Scan";
import { ChatRoomScreen } from "./views/ChatRoom";

import { RootStackParamsList } from "./Interfaces/router";

const Stack = createNativeStackNavigator<RootStackParamsList>();

const Tab = createMaterialBottomTabNavigator();

function Home() {
  return (
    <Tab.Navigator
      activeColor="#FFF"
      inactiveColor="#FFF"
      barStyle={{ backgroundColor: "#000" }}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color }) => (
            <Icon style={styles.icon} fill="#FFF" name="home" />
          ),
        }}
      />
      {/* <Tab.Screen name="Chat" component={ChatScreen} /> */}
      <Tab.Screen
        name="Post"
        component={PostScreen}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color }) => (
            <Icon style={styles.icon} fill="#FFF" name="plus-square" />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color }) => (
            <Icon style={styles.icon} fill="#FFF" name="person" />
          ),
        }}
      />
      <Tab.Screen
        name="Scan"
        component={ScanScreen}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color }) => (
            <Icon style={styles.icon} fill="#FFF" name="stop-circle-outline" />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default function App() {
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={{ ...eva.dark, ...theme }}>
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name="Navbar" component={Home} />
            <Stack.Screen name="Login">
              {(props) => <LoginScreen {...props} />}
            </Stack.Screen>
            <Stack.Screen name="Register" component={RegisterScreen} />
            <Stack.Screen name="PostPage" component={Post_page} />
            <Stack.Screen name="Map" component={MapScreen} />
            <Stack.Screen name="UserProfile" component={UserProfileScreen} />
            <Stack.Screen name="Chat" component={ChatScreen} />
            <Stack.Screen name="ChatRoom" component={ChatRoomScreen} />
          </Stack.Navigator>
          <StatusBar style="auto" />
        </NavigationContainer>
      </ApplicationProvider>
    </>
  );
}

const styles = StyleSheet.create({
  icon: {
    width: 27,
    height: 27,
    marginBottom: -20,
  },
});
