// -*-coding:utf-8 -*-

//@File    :   Comment.tsx
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


//Import Libs
import { View, Image } from 'react-native';
import { Text } from '@ui-kitten/components';

// Import StyleSheets
import styles from '../StyleSheets/Comment';


export const CommentComponent = (props: any) => {

    return (
        <View style={styles.comments} key={props.id}>
            <Image source={{ uri: (props.uri ? props.uri : 'https://wallpapercave.com/wp/wp4041617.jpg') }} style={styles.profile_pic} />
            <Text style={styles.comment_author}> {props.author_username} </Text>
            <Text style={styles.comment_content}> {props.content} </Text>
        </View> 
    )
}