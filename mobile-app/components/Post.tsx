// -*-coding:utf-8 -*-

//@File    :   Post.tsx
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


//Import Libs
import React, { useEffect } from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import { Layout, Text, Divider, Icon } from '@ui-kitten/components';
import Moment from 'moment';

// Import Services
import { post_data, handle_like } from '../Services/api/post'

// Import Interfaces
import { IPost } from '../Interfaces/post';

// Import Components
import styles from '../StyleSheets/PostComponent';


export const Post = (props: any) => {
    const [isLiked, setIsLiked] = React.useState(false)
    const [post, setPost] = React.useState<IPost | null>(null)

    useEffect(() => {
        const setup = async () => {
            setPost(await post_data(props.data._id.$oid))
            setIsLiked(post ? post.liked : false)
        }
        
        setup()
    }, [])

    return (
        <View>
            <TouchableOpacity onPress={() => {
                props.navigation.push('UserProfile', {
                    data: post && post.author,
                });
            }}>
                <Text style={styles.card_username}>{post ? post.author_username : 'someone'}</Text>
            </TouchableOpacity>

            {props.data.location &&
                <TouchableOpacity onPress={() => {
                    props.navigation.push('Map', {
                        data: post && post.id,
                    });
                }}>
                    {post && post.location.description  && <Text style={styles.card_location}>{post.location.description}</Text> }
                </TouchableOpacity>}


            {post && <Image source={{ uri: post.uri }} style={styles.image} />}
            <Layout style={styles.post_actions}>
                {post && <TouchableOpacity onPress={() => {
                    handle_like(post.id, !isLiked)
                    setIsLiked(!isLiked)
                }}>
                    <Icon
                        style={styles.icon}
                        fill='#FFF'
                        name={post && isLiked ? 'heart' : 'heart-outline'}
                    />
                </TouchableOpacity>}
                <TouchableOpacity onPress={() => {
                    props.navigation.navigate('PostPage', {
                        data: post,
                    });
                }}>
                    <Icon
                        style={styles.icon}
                        fill='#FFF'
                        name='message-square-outline'
                    />
                </TouchableOpacity>
            </Layout>
            <Text style={styles.card_like}>{post ? post.like : 'some likes'} {post && post.like > 1 ? "likes" : "like"}</Text>
            <View style={styles.card_title_content}>
                <Text style={styles.card_title}>{post ? post.title : 'title'}</Text>
                <Text style={styles.card_content}>{post ? post.content : 'content'}</Text>
            </View>
            {post && post.comments && post.comments.length > 0 &&
                <View>
                    <TouchableOpacity onPress={() => {
                        props.navigation.push('PostPage', {
                            data: post,
                        });
                    }}>
                        <Text style={styles.card_comment_number}>View all {post.comments.length} comments</Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.card_comment_username}>{post.comments[0].author_username}</Text>
                        <Text style={styles.card_comment_content}>{post.comments[0].content}</Text>
                    </View>
                </View>
            }
            <Text style={styles.card_date}>{post ? Moment(post.created.$date).format('d MMM YYYY') : 'sometime'} </Text>
            <Divider style={{ backgroundColor: "#222", margin: 15 }} />
        </View>
    )
}