import React, { useEffect } from 'react';
import { BottomNavigationTab, BottomNavigation } from '@ui-kitten/components';
import { Layout, Text, Input, Button } from '@ui-kitten/components';
import * as SecureStore from 'expo-secure-store';


export const BottomBar = ({ navigation }) => {
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  let content

  const Logout = async () => {
    await SecureStore.deleteItemAsync('secureToken');
    navigation.replace('login')
  }

  const HandleClick = (index) => {
    setSelectedIndex(index)

    if (selectedIndex == 2) {
      Logout()
    }
  }

  return (<Layout><BottomNavigation
    selectedIndex={selectedIndex}
    onSelect={index => HandleClick(index)}>
    <BottomNavigationTab title='FEED' />
    <BottomNavigationTab title='ORDERS' />
    <BottomNavigationTab title='LOGOUT' onSelect={Logout} />
  </BottomNavigation></Layout>)
}