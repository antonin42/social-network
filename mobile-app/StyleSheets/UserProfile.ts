// -*-coding:utf-8 -*-

//@File    :   UserProfile.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    layout: {
        flex: 2,
    },
    icon: {
        width: 27,
        height: 27,
        marginTop: 10,
        marginRight: 20,
    },
    header_page: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        width: Dimensions.get('window').width
    },
    header_profile: {
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        alignItems: 'center'
    },
    profile_pic: {
        width: 80,
        height: 80,
        marginLeft: 30,
        borderRadius: 40,
        backgroundColor: "#001525"
    },
    profile_username: {
        marginLeft: 20,
        fontSize: 20,
        fontWeight: 'bold'
    },
    profile_post_number: {
        marginLeft: 20,
        color: "#AAA"
    },
    friend_request: {
        marginTop: 20,
        marginBottom: 10,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    friend_request_btn: {
        marginRight: 40,
        backgroundColor: "#DDD",
        padding: 8,
        paddingLeft: 13,
        paddingRight: 13,
        borderRadius: 10
    },
    friend_request_btn_text: {
        color: "#000",
        fontWeight: 'bold'
    }
});