// -*-coding:utf-8 -*-

//@File    :   CreatePostScreen.ts
//@Author  :   Antonin Alves
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native

import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  layout: {
    flex: 2,
    justifyContent: "space-between",
    alignItems: "center",
  },
  backdrop: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height - 50,
    padding: 20,
  },
  map_btn: {
    height: 50,
    backgroundColor: "#FFF",
    borderColor: "#FFF",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  map_btn_text: {
    color: "#000",
    fontSize: 17,
  },
  title: {
    margin: 0,
    padding: 0,
    paddingTop: 20,
    paddingBottom: 10,
    backgroundColor: "#000",
    borderBottomColor: "#444",
    borderBottomWidth: 0.5,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title_text: {
    marginLeft: 20,
    fontWeight: "bold",
    fontSize: 25,
    marginTop: 2,
  },
  icon: {
    width: 27,
    height: 27,
    marginTop: 10,
    marginRight: 20,
  },
  pick_image: {
    width: Dimensions.get("window").width - 50,
    height: Dimensions.get("window").width - 50,
    borderRadius: 5,
    backgroundColor: "#000",
    borderColor: "#FFF",
    borderWidth: 0.5,
    margin: 10,
  },
  image: {
    width: Dimensions.get("window").width - 51,
    height: Dimensions.get("window").width - 51,
    borderRadius: 6,
  },
  icon_image: {
    width: 50,
    height: 50,
    marginLeft: Dimensions.get("window").width / 2 - 50,
    marginTop: Dimensions.get("window").width / 2 - 50,
  },
  form: {
    flex: 3,
    justifyContent: "flex-start",
    alignItems: "center",
    margin: 20,
  },
  inputs: {
    width: Dimensions.get("window").width - 50,
    margin: 5,
    backgroundColor: "#000",
    borderColor: "#000",
    color: "#FFF",
    borderBottomColor: "#AAA",
    borderRadius: 0,
  },
  btn: {
    width: 150,
    margin: 10,
    padding: 10,
    paddingTop: 7,
    paddingBottom: 7,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFF",
    borderColor: "#FFF",
    borderRadius: 5,
  },
  btn_text: {
    color: "#000",
  },
  bottomBtn: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
});
