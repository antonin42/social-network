// -*-coding:utf-8 -*-

//@File    :   HomeScreen.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    layout: {
        flex: 2,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    title: {
        margin: 0,
        padding: 0,
        paddingTop: 20,
        paddingBottom: 10,
        backgroundColor: "#000",
        borderBottomColor: "#444",
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    title_text: {
        marginLeft: 20,
        fontWeight: 'bold',
        fontSize: 25,
        marginTop: 2
    },
    icon: {
        width: 27,
        height: 27,
        marginTop: 10,
        marginRight: 20,

    },
    card: {
        flex: 1,
        margin: -5,
        padding: 0,
        marginBottom: 10,
        borderColor: 'transparent',
    },
    card_username: {
        margin: 0,
        textTransform: 'capitalize',
        marginLeft: 20,
        marginTop: 10,
        marginBottom: 10,
        fontWeight: 'bold'
    },
    card_title: {
        margin: 0,
        textTransform: 'capitalize',
        marginLeft: 20,
        marginTop: 10,
        fontWeight: 'bold'
    },
    card_content: {
        margin: 0,
        marginBottom: 5,
        marginLeft: 20,
        marginTop: 5,
    }
});