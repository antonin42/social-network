// -*-coding:utf-8 -*-

//@File    :   PostComponent.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    image: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').width,
        margin: 0,
        marginTop: 10
    },
    post_actions: {
        flexDirection: 'row'
    },
    title_text: {
        marginLeft: 20,
        fontWeight: 'bold',
        fontSize: 25,
    },
    icon: {
        width: 27,
        height: 27,
        marginTop: 10,
        marginLeft: 20,
    },
    card_like: {
        margin: 0,
        marginLeft: 20,
        fontWeight: 'bold',
        marginTop: 2
    },
    card_username: {
        marginLeft: 20,
        marginTop: 5,
        marginBottom: 3,
        fontWeight: 'bold'
    },
    card_location: {
        margin: 0,
        fontSize: 13,
        textTransform: 'capitalize',
        marginLeft: 20,
    },
    card_title_content: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    card_title: {
        margin: 0,
        textTransform: 'capitalize',
        marginLeft: 20,
        fontWeight: 'bold'
    },
    card_content: {
        margin: 0,
        marginLeft: 10,
    },
    card_comment_number: {
        margin: 0,
        marginLeft: 20,
        marginTop: 6,
        marginBottom: 2,
        color: "#AAA"
    },
    card_comment_username: {
        margin: 0,
        marginLeft: 20,
        fontWeight: 'bold',
    },
    card_comment_content: {
        margin: 0,
        marginLeft: 10,
    },
    card_date: {
        margin: 0,
        marginLeft: 20,
        marginTop: 10,
        fontSize: 11,
        color: "#AAA"
    }
});
