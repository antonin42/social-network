// -*-coding:utf-8 -*-

//@File    :   PostPage.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  layout: {
    flex: 2,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    margin: 0,
    padding: 0,
    paddingTop: 20,
    paddingBottom: 10,
    backgroundColor: "#000",
    borderBottomColor: "#444",
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    justifyContent: "space-between"
  },
  profile_header: {
    margin: 20
  },
  profile_pic_username: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  profile_pic: {
    width: 50,
    height: 50,
    borderRadius: 40,
    backgroundColor: "#001525"
  },
  profile_username: {
    fontWeight: 'bold',
    marginLeft: 10
  },
  post_title: {
    marginTop: 10,
    fontWeight: 'bold'
  },
  post_content: {
    textTransform: 'capitalize'
  },
  new_comment: {
    flexDirection: 'row',
    width: Dimensions.get('screen').width,
    alignItems: 'center',
    justifyContent: 'center'
  },
  new_comment_input: {
    backgroundColor: "#000",
    width: 250,
    borderColor: "#000",
    borderBottomColor: "#FFF",
    borderRadius: 0,
    marginBottom: 20
  },
  new_comment_icon: {
    width: 27,
    height: 27,
    marginLeft: 20,
    marginBottom: 10
  },
  title_text: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 25,
    marginTop: 2
  },
  icon: {
    width: 27,
    height: 27,
    marginTop: 10,
    marginRight: 20,
  },

  comments: {
    flexDirection: 'row'
  },
  comment_author: {
    marginRight: 10,
    marginLeft: 20,
    textTransform: 'capitalize',
    fontWeight: 'bold'
  }
});