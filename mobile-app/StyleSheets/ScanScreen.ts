import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    backgroundColor: "#000",
  },
  button: {
    backgroundColor: "#000",
    borderColor: "#000",
    borderBottomColor: "#FFF",
  },
});
