import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  layout: {
    height: 500,
  },
  title: {
    margin: 0,
    padding: 0,
    paddingTop: 20,
    paddingBottom: 10,
    backgroundColor: "#000",
    borderBottomColor: "#444",
    borderBottomWidth: 0.5,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title_text: {
    marginLeft: 20,
    fontWeight: "bold",
    fontSize: 25,
    marginTop: 2,
  },
  icon: {
    width: 27,
    height: 27,
    marginTop: 10,
    marginRight: 20,
  },
  header_profile: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    marginBottom: 10,
  },
  profile_pic: {
    width: 80,
    height: 80,
    marginLeft: 30,
    marginTop: 30,
    borderRadius: 40,
    backgroundColor: "#001525",
  },
  icon_image: {
    width: 30,
    height: 30,
    marginLeft: 25,
    marginTop: 25,
  },
  profile_username: {
    marginLeft: 30,
    marginTop: 35,
    fontSize: 20,
    fontWeight: "bold",
  },
  profile_post_number: {
    marginLeft: 30,
    marginTop: 0,
    color: "#AAA",
  },
  profile_email: {
    marginLeft: 30,
    marginTop: 15,
  },
  description_container: {
    padding: 5,
    paddingLeft: 30,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  description_text: {
    marginLeft: 10,
  },
  tabsContainerStyle: {
    backgroundColor: "transparent",
    borderRadius: 0,
  },
  tabStyle: {
    backgroundColor: "transparent",
    borderColor: "transparent",
    borderRadius: 0,
    marginRight: 30,
    marginLeft: 30,
    marginBottom: 5,
  },
  tabTextStyle: {
    color: "#FFF",
    marginBottom: 2,
  },
  activeTabStyle: {
    backgroundColor: "transparent",
    borderBottomColor: "#000",
    borderBottomWidth: 0.5,
    marginRight: 30,
    marginLeft: 30,
  },
  activeTabTextStyle: {
    fontWeight: "bold",
    color: "#FFF",
    marginBottom: 2,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalCloseBtn: {
    backgroundColor: "#000",
    borderColor: "#000",
    marginTop: 20,
  },
});
