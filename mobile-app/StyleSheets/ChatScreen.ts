import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  layout: {
    flex: 2,
    justifyContent: "space-between",
    alignItems: "center",
  },
  title: {
    margin: 0,
    padding: 0,
    paddingTop: 20,
    paddingBottom: 10,
    backgroundColor: "#000",
    borderBottomColor: "#444",
    borderBottomWidth: 0.5,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title_text: {
    marginLeft: 20,
    fontWeight: "bold",
    fontSize: 25,
    marginTop: 2,
  },
  icon: {
    width: 27,
    height: 27,
    marginTop: 10,
    marginRight: 20,
  },
  tabsContainerStyle: {
    backgroundColor: "transparent",
    borderRadius: 0,
  },
  tabStyle: {
    backgroundColor: "transparent",
    borderColor: "transparent",
    borderRadius: 0,
    marginRight: 30,
    marginLeft: 30,
    marginBottom: 5,
  },
  tabTextStyle: {
    color: "#FFF",
    marginBottom: 2,
  },
  activeTabStyle: {
    backgroundColor: "transparent",
    borderBottomColor: "#000",
    borderBottomWidth: 0.5,
    marginRight: 30,
    marginLeft: 30,
  },
  activeTabTextStyle: {
    fontWeight: "bold",
    color: "#FFF",
    marginBottom: 2,
  },
  friend_request_container: {
    padding: 20,
  },
  friend_request_title: {
    fontWeight: "bold",
  },
  friends_container: {
    padding: 20,
  },
  friends_title: {
    fontWeight: "bold",
  },
  profile_container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  profile_header: {
    margin: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  profile_pic: {
    width: 35,
    height: 35,
    borderRadius: 40,
    backgroundColor: "#001525",
  },
  profile_username: {
    fontWeight: "bold",
    marginLeft: 10,
  },
});
