// -*-coding:utf-8 -*-

//@File    :   MapScreen.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        width: Dimensions.get('window').width,
        height: 150,
        backgroundColor: '#000',
        borderColor: '#000',
        borderRadius: 0,
        paddingTop: 35,
        paddingLeft: 30
    },
    post_title: {
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 5,
        marginLeft: 15
    },
    post_author: {
        fontSize: 15,
        marginLeft: 40

    },
    post_location_icon: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
    },
    post_location: {
        fontSize: 15
    },
    post_icon: {
        width: 27,
        height: 27,
        padding: 20,
        borderRadius: 30,
        borderColor: "#FFF",
        marginLeft: 0,
        marginRight: 20
    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - 180,
    },
    map_btn: {
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#000',
        borderColor: '#000',
        borderRadius: 0
    },
    map_btn_icon: {
        width: 27,
        height: 27,
        marginLeft: 0,
        marginRight: 5
    }
});