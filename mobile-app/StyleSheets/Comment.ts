// -*-coding:utf-8 -*-

//@File    :   Comment.ts
//@Author  :   Antonin Alves 
//@Version :   1.0
//@Contact :   antonin@alors-la.center
//@License :   GNU Lesser General Public License(LGPL)
//@Desc    :   M - Social Network App with React Native


import { Dimensions, StyleSheet } from 'react-native';

export default StyleSheet.create({
    comments: {
        width: Dimensions.get("screen").width,
        flexDirection: 'row',
        alignItems: 'center',
        margin: 5,
        marginLeft: 20
    },
    profile_pic: {
        width: 35,
        height: 35,
        borderRadius: 40,
        backgroundColor: "#001525"
    },
    comment_author: {
        fontWeight: 'bold',
        margin: 0,
        marginLeft: 5,
        textTransform: 'capitalize'
    },
    comment_content: {
        margin: 0,
        marginLeft: 5
    }
})