import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
  },
  layout: {
    flex: 2,
    justifyContent: "space-between",
    alignItems: "center",
  },
  header: {
    flex: 2,
    justifyContent: "center",
  },
  headerText: {
    fontWeight: "bold",
    fontSize: 24,
  },
  form: {
    flex: 3,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  inputs: {
    width: Dimensions.get("screen").width / 1.5,
    margin: 10,
  },
  formBtn: {
    width: Dimensions.get("screen").width / 3,
    margin: 10,
    backgroundColor: "#EEE",
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: "center",
    borderRadius: 6,
  },
  formBtnText: {
    color: "#000",
    fontWeight: "bold",
  },
  bottomBtnContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  bottomBtn: {
    margin: 10,
  },
  bottomBtnText: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
});
