
from datetime import timedelta

from flask import Flask
from flask_mongoengine import MongoEngine
from app.routes.router import router
from flask_jwt_extended import JWTManager
from flask_bcrypt import Bcrypt

from config import MONGODB_CONNSTRING, JWT_SECRET_KEY


app = Flask(__name__)
bcrypt = Bcrypt(app)

app.register_blueprint(router)


app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(days=30)
app.config["JWT_REFRESH_TOKEN_EXPIRES"] = timedelta(days=30)

if JWT_SECRET_KEY:
    app.config['JWT_SECRET_KEY'] = JWT_SECRET_KEY
    jwt = JWTManager(app)


if MONGODB_CONNSTRING:

    app.config['MONGODB_SETTINGS'] = [
        {
            'host': MONGODB_CONNSTRING
        }]
    db = MongoEngine()
    db.init_app(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000, debug=True)
