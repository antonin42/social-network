from datetime import datetime
from tokenize import Floatnumber
from mongoengine import Document, IntField
from mongoengine import DateTimeField, EmbeddedDocument, FloatField
from mongoengine.fields import StringField, EmbeddedDocumentField, ImageField, ListField


class Post_comment(EmbeddedDocument):
    author = StringField(required=True)
    author_username = StringField(required=True)
    content = StringField(required=True)


class Post_location(EmbeddedDocument):
    description = StringField(required=False)
    latitude = FloatField(require=False)
    longitude = FloatField(required=False)


class Post(Document):
    title = StringField(required=True)
    author = StringField(required=True)
    author_username = StringField(required=True)
    uri = StringField(required=True)
    content = StringField(required=True)
    like = IntField(default=0)
    comments = ListField(EmbeddedDocumentField(Post_comment))
    location = EmbeddedDocumentField(Post_location)
    created = DateTimeField(default=datetime.now)
    meta = {'collection': 'posts'}
