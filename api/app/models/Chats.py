from datetime import datetime
from tokenize import Floatnumber
from mongoengine import Document, IntField
from mongoengine import DateTimeField, EmbeddedDocument, FloatField
from mongoengine.fields import StringField, EmbeddedDocumentField, ImageField, ListField


class Messages(EmbeddedDocument):
    username = StringField(required=False)
    user_id = StringField(required=False)
    user_uri = StringField(required=False)
    content = StringField(required=False)
    created = DateTimeField(default=datetime.now)


class Chat(Document):
    users = ListField(StringField(required=True))
    messages = ListField(EmbeddedDocumentField(Messages))
    created = DateTimeField(default=datetime.now)
    meta = {'collection': 'chat'}
