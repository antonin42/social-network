from datetime import datetime
from mongoengine import Document, EmbeddedDocument
from mongoengine import DateTimeField
from mongoengine.fields import BooleanField, EmbeddedDocumentField, ImageField, ListField, MapField, ReferenceField, EmailField, StringField
from app.main import MONGO_DB_COMMON_ALIAS
from app.main import bcrypt


class Users_friendRequest(EmbeddedDocument):
    user_id = StringField(required=False)
    username = StringField(required=False)
    created = DateTimeField(default=datetime.now)


class Users_friend(EmbeddedDocument):
    user_id = StringField(required=False)
    username = StringField(required=False)
    created = DateTimeField(default=datetime.now)


class Users_account(EmbeddedDocument):
    is_active = BooleanField(default=False)
    is_email_verified = BooleanField(default=False)
    is_banned = BooleanField(default=False)
    is_admin = BooleanField(default=False)


class Users(Document):
    email = EmailField(unique=True, required=True, max_length=75)
    username = StringField(unique=True, required=True,
                           min_length=2, max_length=20)
    password = StringField(required=True, min_length=8, max_length=145)
    admin = BooleanField(default=False)
    account = EmbeddedDocumentField(Users_account)
    uri = StringField(required=True)
    description = StringField(default="...")
    likes = ListField(StringField())
    friend_requests = ListField(EmbeddedDocumentField(Users_friendRequest))
    friend = ListField(EmbeddedDocumentField(Users_friend))
    created = DateTimeField(default=datetime.now)
    meta = {'collection': 'users'}

    @staticmethod
    def password_strength():
        return r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]"

    def password_hash(self):
        self.password = bcrypt.generate_password_hash(
            self.password).decode('utf8')

    def password_check(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def verify_email(self):
        self.account.is_email_verified = True
        self.save()

    def unverified_email(self):
        self.account.is_email_verified = False
        self.save()
