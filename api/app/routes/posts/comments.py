import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post, Post_comment
from app.models.Users import Users
from app.errors import *
import logging

posts_comment = Blueprint('posts_comment', __name__)


@posts_comment.route('/<post_id>/comment', methods=['POST'])
@jwt_required()
def comment_post_(post_id):
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()
    body = request.get_json()
    if not 'content' in body:
        return jsonify({'error': 'missing content'}), 403
    try:
        post_comment = Post_comment(
            author=user_id,
            author_username=user.username,
            content=body.get('content')
        )
        Post.objects(id=post_id).update(push__comments=post_comment)

        return jsonify({"msg": "You comment a post"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403


@posts_comment.route('/<post_id>/comments', methods=['GET'])
@jwt_required()
def get_post_comments_(post_id):
    try:
        post = Post.objects(id=post_id).first()

        if post.comments:
            return jsonify(post.comments), 200
        else:
            return jsonify({'msg': 'no comments'})
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403

#
