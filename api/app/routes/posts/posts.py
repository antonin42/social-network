import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post, Post_location
from app.models.Users import Users
from app.errors import *
import logging
import json

posts = Blueprint('posts', __name__)
logger = logging.getLogger('werkzeug')


@posts.route('/', methods=['POST'])
@jwt_required()
def create_post_():
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    body = request.get_json()
    if not 'title' in body:
        return missing_arg_title(), 400
    if not 'content' in body:
        return missing_arg_content(), 400
    if not 'uri' in body:
        return missing_arg_uri(), 400
    try:
        if 'location' in body:
            location_post = Post_location(description=body.get('location'),
                                          latitude=body.get('latitude'),
                                          longitude=body.get('longitude'))

            new_post = Post(title=body.get('title'),
                            uri=body.get('uri'),
                            author=user_id,
                            author_username=user.username,
                            location=location_post,
                            content=body.get('content'))
        else:
            new_post = Post(title=body.get('title'),
                            uri=body.get('uri'),
                            author=user_id,
                            author_username=user.username,
                            content=body.get('content'))

        new_post.validate()
        new_post.save()

        return jsonify({"msg": "Post successfully created"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403


@posts.route('/')
@jwt_required()
def get_posts():
    logger.info("there")
    posts = Post.objects().limit(20)
    logger.info("there")
    return jsonify(posts)


@posts.route('/<post_id>', methods=['GET'])
@jwt_required()
def get_one_post(post_id):
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    post = Post.objects(id=post_id).first()
    logger.info(post)
    liked = False
    for item in user.likes:
        if item == post_id:
            liked = True

    return jsonify(id=post_id,
                   title=post.title,
                   author=post.author,
                   author_username=post.author_username,
                   uri=post.uri,
                   content=post.content,
                   created=post.created,
                   like=post.like,
                   comments=post.comments,
                   location=post.location,
                   liked=liked)
