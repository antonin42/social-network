import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post
from app.models.Users import Users
from app.errors import *
import logging

posts_like = Blueprint('posts_like', __name__)


@posts_like.route('/<post_id>/like', methods=['PUT'])
@jwt_required()
def like_post_(post_id):
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    try:
        if not post_id in user.likes:
            Post.objects(id=post_id).update_one(inc__like=1)
            user.update(push__likes=post_id)
        else:
            return jsonify({"msg": "You already like this post"}), 400

        return jsonify({"msg": "You liked a post"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403


@posts_like.route('/<post_id>/dislike', methods=['PUT'])
@jwt_required()
def dislike_post_(post_id):
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    try:
        if post_id in user.likes:
            Post.objects(id=post_id).update_one(dec__like=1)
            user.update(pull__likes=post_id)
        else:
            return jsonify({"msg": "You didn't like this post"}), 400

        return jsonify({"msg": "You disliked a post"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403


@posts_like.route('/likes', methods=['GET'])
@jwt_required()
def get_posts():
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    posts = []
    for item in user.likes:
        posts.append(Post.objects(id=item).first())

    print()
    return jsonify(posts)
