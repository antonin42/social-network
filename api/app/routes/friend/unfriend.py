import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post
from app.models.Users import Users
from app.errors import *
import logging

unfriend = Blueprint('unfriend', __name__)


@unfriend.route('/<target_user_id>', methods=['PUT'])
@jwt_required()
def unfriend_(target_user_id):
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    try:
        target_user = Users.objects(id=target_user_id).first()
        target_user.update(pull__friend__user_id=user_id)
        user.update(pull__friend__user_id=target_user_id)

        return jsonify({"msg": "You unfriend somenone"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403
