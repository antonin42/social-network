import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post, Post_comment
from app.models.Users import Users, Users_friendRequest
from app.errors import *
import logging

friend_request = Blueprint('friend_request', __name__)


@friend_request.route('/<user_id>', methods=['POST'])
@jwt_required()
def make_friend_request_(user_id):
    current_user_id = get_jwt_identity()
    current_user = Users.objects(id=current_user_id).first()
    try:
        # target_user = Users.objects(id=user_id).first()

        friend_request = Users_friendRequest(
            user_id=current_user_id,
            username=current_user.username
        )

        Users.objects(id=user_id).update(push__friend_requests=friend_request)

        return jsonify({"msg": "You request a friend"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403
