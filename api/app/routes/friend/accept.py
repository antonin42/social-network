import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post, Post_comment
from app.models.Users import Users, Users_friendRequest, Users_friend
from app.errors import *
import logging

accept_request = Blueprint('accept_request', __name__)


@accept_request.route('/<user_id>', methods=['POST'])
@jwt_required()
def accept_friend_request_(user_id):
    current_user_id = get_jwt_identity()
    try:
        target_user = Users.objects(id=user_id).first()
        current_user = Users.objects(id=current_user_id).first()

        Users.objects(id=current_user_id).update(
            pull__friend_requests__user_id=user_id)

        target_friend = Users_friend(
            user_id=current_user_id,
            username=current_user.username
        )

        current_friend = Users_friend(
            user_id=user_id,
            username=target_user.username
        )

        Users.objects(id=current_user_id).update(
            push__friend=current_friend)

        Users.objects(id=user_id).update(
            push__friend=target_friend)

        return jsonify({"msg": "You request a friend"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403
