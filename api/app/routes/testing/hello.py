from flask import jsonify, Blueprint

testing = Blueprint('testing', __name__)


@testing.route('/hello', methods=['GET'])
def hello_():
    return jsonify({'msg': 'Hi, what can I do for you ?'}), 200
