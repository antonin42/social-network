import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Chats import Chat, Messages
from app.models.Users import Users
from app.errors import *

send_message = Blueprint('send_message', __name__)


@send_message.route('/<target_user_id>', methods=['POST'])
@jwt_required()
def send_message_(target_user_id):
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    body = request.get_json()
    try:
        new_message = Messages(username=user.username,
                               user_id=user_id,
                               user_uri=user.uri,
                               content=body.get('content'))

        current_chat = Chat.objects(users=[user_id, target_user_id]).first()
        if not current_chat:
            current_chat = Chat.objects(
                users=[target_user_id, user_id]).first()

        if current_chat:
            current_chat.update(push__messages=new_message)
        else:
            new_chat = Chat(users=[target_user_id, user_id],
                            messages=[new_message])
            new_chat.validate()
            new_chat.save()

        return jsonify({"msg": "Message successfully sended"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403
