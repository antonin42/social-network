import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Chats import Chat
from app.models.Users import Users
from app.errors import *

message_list = Blueprint('message_list', __name__)


@message_list.route('/<target_user_id>', methods=['GET'])
@jwt_required()
def get_one_post(target_user_id):
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    current_chat = Chat.objects(users=[user_id, target_user_id]).first()
    if not current_chat:
        current_chat = Chat.objects(
            users=[target_user_id, user_id]).first()

    if not current_chat:
        return jsonify({"error": "You didn't start chating with this user :)"})

    return jsonify(current_chat)
