
import re
from flask import Blueprint

from app.routes.testing.hello import testing
from app.routes.authentication.login import login
from app.routes.authentication.register import register
from app.routes.posts.posts import posts
from app.routes.users.posts import users_posts
from app.routes.posts.likes import posts_like
from app.routes.posts.comments import posts_comment
from app.routes.users.me import me
from app.routes.users.other_users import other_users
from app.routes.friend.request import friend_request
from app.routes.friend.accept import accept_request
from app.routes.friend.unfriend import unfriend
from app.routes.chat.sendMessage import send_message
from app.routes.chat.messageList import message_list

router = Blueprint('router', __name__)

router.register_blueprint(testing)
router.register_blueprint(login, url_prefix='/users')
router.register_blueprint(register, url_prefix='/users')
router.register_blueprint(users_posts, url_prefix='/users')
router.register_blueprint(me, url_prefix='/users')
router.register_blueprint(other_users, url_prefix='/users')
router.register_blueprint(posts, url_prefix='/posts')
router.register_blueprint(posts_like, url_prefix='/posts')
router.register_blueprint(posts_comment, url_prefix='/posts')
router.register_blueprint(friend_request, url_prefix='/friends/request')
router.register_blueprint(accept_request, url_prefix='/friends/accept')
router.register_blueprint(unfriend, url_prefix='/friends/unfriend')
router.register_blueprint(send_message, url_prefix='/chat')
router.register_blueprint(message_list, url_prefix='/chat')
