import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post
from app.models.Users import Users
from app.errors import *


me = Blueprint('me', __name__)


@me.route('/me/', methods=['GET'])
@jwt_required()
def get_my_data_():
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    return jsonify(id=user_id,
                   email=user.email,
                   username=user.username,
                   description=user.description,
                   uri=user.uri
                   )


@me.route('/me', methods=['PUT'])
@jwt_required()
def edit_my_data_():
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()
    body = request.get_json()

    try:
        if body.get('uri'):
            user.update(uri=body.get('uri'))
        if body.get('description'):
            user.update(description=body.get('description'))
        if not body.get('description') and not body.get('uri'):
            return jsonify({"msg": "You have to add params"}), 400

        return jsonify({"msg": "You have edited your profile"}), 200
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403


@me.route('/me/friendRequests', methods=['GET'])
@jwt_required()
def get_my_friend_requests_():
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    return jsonify(user.friend_requests)


@me.route('/me/friends', methods=['GET'])
@jwt_required()
def get_my_friend_():
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()

    return jsonify(user.friend)
