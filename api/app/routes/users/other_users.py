import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post
from app.models.Users import Users
from app.errors import *


other_users = Blueprint('other_users', __name__)


@other_users.route('/<target_user_id>', methods=['GET'])
@jwt_required()
def get_users_data_(target_user_id):
    user_id = get_jwt_identity()
    user = Users.objects(id=user_id).first()
    is_friend = False
    try:
        target_user = Users.objects(id=target_user_id).first()
        for item in user.friend:
            if item.user_id == target_user_id:
                is_friend = True
        return jsonify(id=target_user_id,
                       account=target_user.account,
                       username=target_user.username,
                       is_friend=is_friend
                       )
    except ValidationError as error:
        return jsonify({'error': error._format_errors()}), 403


@other_users.route('/<user_id>/posts/', methods=['GET'])
@jwt_required()
def get_user_posts(user_id):

    posts = Post.objects(author=user_id)

    return jsonify(posts)
