import re
import datetime
from flask import request, jsonify, Blueprint
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from mongoengine.errors import ValidationError
from app.models.Posts import Post
from app.models.Users import Users
from app.errors import *


users_posts = Blueprint('users_posts', __name__)


@users_posts.route('/me/posts/', methods=['GET'])
@jwt_required()
def get_my_posts():
    user_id = get_jwt_identity()

    posts = Post.objects(author=user_id)

    return jsonify(posts)
