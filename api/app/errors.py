
# REGISTER

def email_already_exists():
    return {"error": "Resource already exists: ['email']"}


def email_does_not_exist():
    return {"error": "Resource does not exist: ['email']"}


def username_already_exists():
    return {"error": "Resource already exists: ['username']"}

# LOGIN


def password_invalid():
    return {"error": "At least 1 uppercase, 1 lowercase, 1 number, 1 special: ['password']"}


def register_field_invalid():
    return {"error": "Invalid resource provided, expect: ['email', 'username', 'password']"}


def login_field_invalid():
    return {"error": "Invalid resource provided, expect: ['email', 'password']"}

# TOKEN


def token_identity_invalid():
    return {"error": "Invalid resource provided ['token']"}

# POSTS


def missing_arg_title():
    return {"error": "missing argument ['title']"}


def missing_arg_author():
    return {"error": "missing argument ['author']"}


def missing_arg_content():
    return {"error": "missing argument ['content']"}


def missing_arg_uri():
    return {"error": "missing argument ['uri']"}
