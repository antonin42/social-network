import os

MONGODB_CONNSTRING = os.environ.get('MONGODB_CONNSTRING')
JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
